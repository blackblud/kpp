package threading;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ThreadPool {

    private ThreadPoolExecutor threadPoolExecutor;

    public ThreadPool(int size) {
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(size);
    }

    public void execute(Runnable command) {
        threadPoolExecutor.execute(command);
    }

    public List<Runnable> shutdownNow(){
        return threadPoolExecutor.shutdownNow();
    }
}
