package sample;

public class Car {
    private String id;
    private Engine engine;
    private Body body;
    private Accessory accessory;

    public Car(String id, Engine engine, Body body, Accessory accessory) {
        this.id = id;
        this.engine = engine;
        this.body = body;
        this.accessory = accessory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Accessory getAccessory() {
        return accessory;
    }

    public void setAccessory(Accessory accessory) {
        this.accessory = accessory;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id='" + id + '\'' +
                ", engine=" + engine +
                ", body=" + body +
                ", accessory=" + accessory +
                '}';
    }
}
