package sample;

public class Body {
    private String id;

    public Body(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Body{" +
                "id='" + id + '\'' +
                '}';
    }
}
