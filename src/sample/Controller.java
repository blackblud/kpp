package sample;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class Controller {

    @FXML
    private Slider engineSlider;

    @FXML
    private Slider bodySlider;

    @FXML
    private Slider accessorySlider;

    @FXML
    private Slider dealerSlider;

    @FXML
    private Button startButton;

    @FXML
    private Button stopButton;

    @FXML
    private Label engineSupplierAmountLabel;

    @FXML
    private Label bodySupplierAmountLabel;

    @FXML
    private Label accessorySupplierAmountLabel;

    @FXML
    private Label carAssemblersAmountLabel;

    @FXML
    private Label dealerAmountLabel;

    @FXML
    private Label engineSuppliedLabel;

    @FXML
    private Label bodySuppliedLabel;

    @FXML
    private Label accessorySuppliedLabel;

    @FXML
    private Label carsAssembledLabel;

    @FXML
    private Canvas engineCanvas;

    @FXML
    private Canvas bodyCanvas;

    @FXML
    private Canvas accessoryCanvas;

    @FXML
    private Canvas carCanvas;

    private static boolean toBeOrNotToBe = true;

    private static int engineSupplierWait = 2000;
    private static int bodySupplierWait = 2000;
    private static int accessorySupplierWait = 2000;
    private static int dealerWait = 2000;

    private static int engineSupplied = 0;
    private static int bodySupplied = 0;
    private static int accessorySupplied = 0;
    private static int carsAssembled = 0;

    private static Image engImage;
    private static Image bodyImage;
    private static Image accessoryImage;
    private static Image carImage;

    private static int engImg = 0;
    private static int bodyImg = 0;
    private static int accessoryImg = 0;
    private static int carImg = 0;

    public static Logger logger = Logger.getGlobal();
    public static FileHandler fileHandler;

    static {
        try {
            fileHandler = new FileHandler("Log.log");
            logger.addHandler(fileHandler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void initialize() {
        try {
            stopButton.setOnAction(event -> {
                toBeOrNotToBe = false;
                fileHandler.close();
            });

            engineSlider.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    engineSupplierWait = (int)engineSlider.getValue()*1000;
                }
            });

            bodySlider.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    bodySupplierWait = (int)bodySlider.getValue()*1000;
                }
            });

            accessorySlider.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    accessorySupplierWait = (int)accessorySlider.getValue()*1000;
                }
            });

            dealerSlider.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    dealerWait = (int)dealerSlider.getValue()*1000;
                }
            });

            ArrayBlockingQueue<Engine> enginesQueue;
            ArrayBlockingQueue<Body> bodiesQueue;
            ArrayBlockingQueue<Accessory> accessoriesQueue;
            ArrayBlockingQueue<Car> carsQueue;

            ArrayList<Thread> engineSuppliers;
            ArrayList<Thread> bodySuppliers;
            ArrayList<Thread> accessorySuppliers;
            ArrayList<Thread> dealers;

            Scanner scanner = new Scanner(new File("configuration.txt"));

            int engineStorage = scanner.nextInt();
            int bodyStorage = scanner.nextInt();
            int accessoriesStorage = scanner.nextInt();
            int carStorage = scanner.nextInt();

            int engineSupplier = scanner.nextInt();
            int bodySupplier = scanner.nextInt();
            int accessorySupplier = scanner.nextInt();
            int carAssembler = scanner.nextInt();
            int carDealers = scanner.nextInt();

            engineSupplierWait = scanner.nextInt()*1000;
            bodySupplierWait = scanner.nextInt()*1000;
            accessorySupplierWait = scanner.nextInt()*1000;
            dealerWait = scanner.nextInt()*1000;

            scanner.close();

            engineSupplierAmountLabel.setText("Кількість постачальників двигунів: " + engineSupplier);
            bodySupplierAmountLabel.setText("Кількість постачальників кузовів: " + bodySupplier);
            accessorySupplierAmountLabel.setText("Кількість постачальників аксесуарів: " + accessorySupplier);
            carAssemblersAmountLabel.setText("Кількість складальників автомобілів: " + carAssembler);
            dealerAmountLabel.setText("Кількість дилерів: " + carDealers);

            enginesQueue = new ArrayBlockingQueue<>(engineStorage);
            bodiesQueue = new ArrayBlockingQueue<>(bodyStorage);
            accessoriesQueue = new ArrayBlockingQueue<>(accessoriesStorage);
            carsQueue = new ArrayBlockingQueue<>(carStorage);

            engineSuppliers = new ArrayList<>(engineSupplier);
            bodySuppliers = new ArrayList<>(bodySupplier);
            accessorySuppliers = new ArrayList<>(accessorySupplier);
            dealers = new ArrayList<>(carDealers);

            for(int i = 0; i < engineSupplier; i++) {
                engineSuppliers.add(new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while (toBeOrNotToBe) {
                                Engine engine = new Engine(Utils.generateRandomAlphanumericString(12));
                                enginesQueue.offer(engine, 1, TimeUnit.MINUTES);
                                incrementEngineSupplied();
                                Thread.sleep(engineSupplierWait);
                            }
                            System.out.println("Engine thread has stopped");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }));
            }

            for(int i = 0; i < bodySupplier; i++) {
                bodySuppliers.add(new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while (toBeOrNotToBe) {
                                Body body = new Body(Utils.generateRandomAlphanumericString(12));
                                bodiesQueue.offer(body, 1, TimeUnit.MINUTES);
                                incrementBodySupplied();
                                Thread.sleep(bodySupplierWait);
                            }
                            System.out.println("Body thread has stopped");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }));
            }

            for(int i = 0; i < accessorySupplier; i++) {
                accessorySuppliers.add(new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while(toBeOrNotToBe) {
                                Accessory accessory = new Accessory(Utils.generateRandomAlphanumericString(12));
                                accessoriesQueue.offer(accessory, 1, TimeUnit.MINUTES);
                                incrementAccessorySupplied();
                                Thread.sleep(accessorySupplierWait);
                            }
                            System.out.println("Accessory thread has stopped");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }));
            }

            for(int i = 0; i < carDealers; i++) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while (toBeOrNotToBe) {
                                carsQueue.take();
                                Thread.sleep(dealerWait);
                            }
                            System.out.println("Dealer thread has stopped");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                dealers.add(thread);
            }

            ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(carAssembler);

            Thread overseer = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (toBeOrNotToBe) {
                        if (carsQueue.remainingCapacity() > 0) {
                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Engine engine = enginesQueue.take();
                                        Body body = bodiesQueue.take();
                                        Accessory accessory = accessoriesQueue.take();
                                        Car car = new Car(Utils.generateRandomAlphanumericString(12), engine, body, accessory);
                                        logger.info(car.toString());
//                                        System.out.println(car);
                                        carsQueue.offer(car, 1, TimeUnit.SECONDS);
                                        incrementCarsAssembled();
                                    } catch (InterruptedException e) { }
                                }
                            });
                        }
                    }
                    executor.shutdownNow();
                    System.out.println("Overseer thread has stopped");
                }
            });

            engImage = SwingFXUtils.toFXImage(Utils.scaleImage(SwingFXUtils.fromFXImage(new Image("engine.jpg"), null),
                    (int)(engineCanvas.getWidth()/engineStorage), (int)(engineCanvas.getHeight())), null);
            bodyImage = SwingFXUtils.toFXImage(Utils.scaleImage(SwingFXUtils.fromFXImage(new Image("body.jpg"), null),
                    (int)(bodyCanvas.getWidth()/bodyStorage), (int)(bodyCanvas.getHeight())), null);
            //fix image
            accessoryImage = SwingFXUtils.toFXImage(Utils.scaleImage(SwingFXUtils.fromFXImage(new Image("engine.jpg"), null),
                    (int)(accessoryCanvas.getWidth()/accessoriesStorage), (int)(accessoryCanvas.getHeight())), null);
            carImage = SwingFXUtils.toFXImage(Utils.scaleImage(SwingFXUtils.fromFXImage(new Image("car.jpg"), null),
                    (int)(carCanvas.getWidth()/accessoriesStorage), (int)(carCanvas.getHeight())), null);

            //tuta
            Thread engineThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(toBeOrNotToBe) {
                        int n = enginesQueue.size() - enginesQueue.remainingCapacity();
                        if(n != engImg) {
                            engImg = n;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    engineCanvas.getGraphicsContext2D().clearRect(0, 0, engineCanvas.getWidth(), engineCanvas.getHeight());
//                                    for(int i = 0; i < (int)(Math.ceil(n/Math.sqrt(engineStorage))); i++) {
//                                        for(int j = 0; j < (int)(Math.floor(n/Math.sqrt(engineStorage))); j++) {
//                                            engineCanvas.getGraphicsContext2D().drawImage(engImage, j*engImage.getWidth(), i*engImage.getHeight());
//                                        }
//                                    }
                                    for(int i = 0; i < n; i++) {
                                        engineCanvas.getGraphicsContext2D().drawImage(engImage, i*engImage.getWidth(),0);
                                    }
                                }
                            });
                        }
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            Thread bodyThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(toBeOrNotToBe) {
                        int n = bodiesQueue.size() - bodiesQueue.remainingCapacity();
                        if(n != bodyImg) {
                            bodyImg = n;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    bodyCanvas.getGraphicsContext2D().clearRect(0, 0, bodyCanvas.getWidth(), bodyCanvas.getHeight());
//                                    for(int i = 0; i < (int)(Math.ceil(n/Math.sqrt(bodyStorage))); i++) {
//                                        for(int j = 0; j < (int)(Math.floor(n/Math.sqrt(bodyStorage))); j++) {
//                                            bodyCanvas.getGraphicsContext2D().drawImage(bodyImage, j*bodyImage.getWidth(), i*bodyImage.getHeight());
//                                        }
//                                    }
                                    for(int i = 0; i < n; i++) {
                                        bodyCanvas.getGraphicsContext2D().drawImage(bodyImage, i*bodyImage.getWidth(),0);
                                    }
                                }
                            });
                        }
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            Thread accessoryThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(toBeOrNotToBe) {
                        int n = accessoriesQueue.size() - accessoriesQueue.remainingCapacity();
                        if(n != accessoryImg) {
                            accessoryImg = n;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    accessoryCanvas.getGraphicsContext2D().clearRect(0, 0, accessoryCanvas.getWidth(), accessoryCanvas.getHeight());
//                                    for(int i = 0; i < (int)(Math.ceil(n/Math.sqrt(accessoriesStorage))); i++) {
//                                        for(int j = 0; j < (int)(Math.floor(n/Math.sqrt(accessoriesStorage))); j++) {
//                                            accessoryCanvas.getGraphicsContext2D().drawImage(accessoryImage, j*accessoryImage.getWidth(), i*accessoryImage.getHeight());
//                                        }
//                                    }
                                    for(int i = 0; i < n; i++) {
                                        accessoryCanvas.getGraphicsContext2D().drawImage(accessoryImage, i*accessoryImage.getWidth(),0);
                                    }
                                }
                            });
                        }
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            Thread carThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(toBeOrNotToBe) {
                        int n = carsQueue.size() - carsQueue.remainingCapacity();
                        if(n != carImg) {
                            carImg = n;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    carCanvas.getGraphicsContext2D().clearRect(0, 0, carCanvas.getWidth(), carCanvas.getHeight());
//                                    for(int i = 0; i < (int)(Math.ceil(n/Math.sqrt(carStorage))); i++) {
//                                        for(int j = 0; j < (int)(Math.floor(n/Math.sqrt(carStorage))); j++) {
//                                            carCanvas.getGraphicsContext2D().drawImage(carImage, j*carImage.getWidth(), i*carImage.getHeight());
//                                        }
//                                    }
                                    for(int i = 0; i < n; i++) {
                                        carCanvas.getGraphicsContext2D().drawImage(carImage, i*carImage.getWidth(),0);
                                    }
                                }
                            });
                        }
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            Thread labelThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(toBeOrNotToBe) {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                engineSuppliedLabel.setText("Постачено двигунів: " + engineSupplied);
                                bodySuppliedLabel.setText("Постачено кузовів: " + bodySupplied);
                                accessorySuppliedLabel.setText("Постачено аксесуарів: " + accessorySupplied);
                                carsAssembledLabel.setText("Складено автомобілів: " + carsAssembled);
                            }
                        });
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            startButton.setOnAction(event -> {
                engineSuppliers.forEach(x -> x.start());
                bodySuppliers.forEach(x -> x.start());
                accessorySuppliers.forEach(x -> x.start());
                dealers.forEach(x -> x.start());

                overseer.start();

                engineThread.start();
                bodyThread.start();
                accessoryThread.start();
                carThread.start();

                labelThread.start();
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void incrementEngineSupplied() {
        engineSupplied++;
    }

    private synchronized void incrementBodySupplied() {
        bodySupplied++;
    }

    private synchronized void incrementAccessorySupplied() {
        accessorySupplied++;
    }

    private synchronized void incrementCarsAssembled() {
        carsAssembled++;
    }

}
