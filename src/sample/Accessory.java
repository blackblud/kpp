package sample;

public class Accessory {
    private String id;

    public Accessory(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Accessory{" +
                "id='" + id + '\'' +
                '}';
    }
}
