package sample;

public class Engine {
    private String id;

    public Engine(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "id='" + id + '\'' +
                '}';
    }
}
